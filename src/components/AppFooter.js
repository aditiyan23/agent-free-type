import React from "react";
import Container from "react-bootstrap/Container";


function AppFooter() {
    return (
        <span style={{paddingLeft:'20px', paddingBottom:'50px', marginTop:'180px'}}>© 2020 Bayu Buana Travel Services. All Rights Reserved.</span>
    );
}

export default AppFooter;
