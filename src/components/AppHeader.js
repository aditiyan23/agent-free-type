import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';
import {ReactComponent as ReactLogo} from '../assets/images/logo.svg';
import {faBell, faBriefcase, faQuestionCircle} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import React from "react";


function AppHeader() {
    return (
        <Navbar bg="light" className="navbar navbar-expand-lg navbar-light bg-light border-bottom" expand="lg">
            <Navbar.Brand className="main-content" href="#/master-data-management/fee-type/list"> <ReactLogo height={50} /></Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav"/>
            <Navbar.Collapse id="basic-navbar-nav" style={{paddingRight: "5%"}}>
                <Nav className="ms-auto">
                    <Nav.Link style={{alignSelf:"center"}}><FontAwesomeIcon style={{fontSize:'22px', color:'#000000'}} icon={faQuestionCircle} className="material-icons"/></Nav.Link>
                    <Nav.Link style={{alignSelf:"center"}}><FontAwesomeIcon style={{fontSize:'22px'}} icon={faBell} className="material-icons"/></Nav.Link>
                    <Nav.Link style={{alignSelf:"center"}}>
                        <img className="rounded-circle" alt="80x80"
                             src="https://picsum.photos/id/3/80/80" data-holder-rendered="true"></img>
                    </Nav.Link>
                    {/*<NavDropdown title="Dropdown" id="basic-nav-dropdown">*/}
                    {/*    <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>*/}
                    {/*    <NavDropdown.Item href="#action/3.2">*/}
                    {/*        Another action*/}
                    {/*    </NavDropdown.Item>*/}
                    {/*    <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>*/}
                    {/*    <NavDropdown.Divider/>*/}
                    {/*    <NavDropdown.Item href="#action/3.4">*/}
                    {/*        Separated link*/}
                    {/*    </NavDropdown.Item>*/}
                    {/*</NavDropdown>*/}
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    );
}

export default AppHeader;
