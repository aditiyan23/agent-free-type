import React from "react";
import classNames from "classnames";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faBriefcase, faHome} from "@fortawesome/free-solid-svg-icons";
import {AppSidebarNav} from "./AppSidebarNav";
import "../assets/css/App.css";


// sidebar nav config
import navigation from '../_nav'
import {Link, NavLink} from "react-router-dom";

class AppSidebar extends React.Component {
    render() {
        let mini = true;

        function toggleSidebar() {
            if (mini) {
                console.log("opening sidebar");

                document.getElementById("mySidebar").style.opacity = "0.9";
                document.getElementById("mySidebar").style.width = "450px";
                // document.getElementById("main").style.marginLeft = "70px";
                mini = false;
            } else {
                console.log("closing sidebar");
                document.getElementById("mySidebar").style.opacity = "1";
                document.getElementById("mySidebar").style.width = "85px";
                // document.getElementById("main").style.marginLeft = "70px";
                mini = true;
            }
        }

        return (
            <div id="mySidebar" className={classNames("sidebar")}  onMouseEnter={toggleSidebar}
                 onMouseLeave={toggleSidebar}>
                <nav>
                    {/*<NavLink*/}
                    {/*    to="/dashboard"*/}
                    {/*    className={({ isActive }) => (isActive ? "link-active" : "link")}*/}
                    {/*>*/}
                    {/*<span>*/}
                    {/*    <FontAwesomeIcon icon={faHome} className="material-icons"/>*/}
                    {/*    <span className="icon-text">Dashboard</span>*/}
                    {/*</span>*/}
                    {/*</NavLink>*/}
                    <NavLink
                        to="/master-data-management/fee-type/list"
                        className={({ isActive }) => (isActive ? "link-active" : "link")}
                    >
                    <span>
                        <FontAwesomeIcon icon={faBriefcase} className="material-icons"/>
                        <span className="icon-text">Master Data Management</span>
                    </span>
                    </NavLink>
                </nav>
            </div>
        );
    }
}

export default AppSidebar;
