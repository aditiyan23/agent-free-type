import React, { Suspense } from 'react'
// import {Container} from "react-bootstrap";
import { Navigate, Route, Routes } from 'react-router-dom'
import { CContainer, CSpinner } from '@coreui/react'
import routes from "../routes";
import AppBreadcrumb from "./AppBreadcrumb";

// class AppContent extends React.Component {
//     render() {
//         return (
//             <Container style={{paddingTop: "23px"}}>
//                 <span>Test</span>
//                 <span>Test</span>
//             </Container>
//         );
//     }
// }

const AppContent = () => {
    return (
        // <CContainer className="m-10 ms-2">
        // </CContainer>
        <div className="m-10 ms-2">
            <Suspense fallback={<CSpinner color="primary" />}>
                <Routes>
                    {routes.map((route, idx) => {
                        return (
                            route.element && (
                                <Route
                                    key={idx}
                                    path={route.path}
                                    exact={route.exact}
                                    name={route.name}
                                    element={<route.element />}
                                />
                            )
                        )
                    })}
                    <Route path="/" element={<Navigate to="/master-data-management/fee-type/list" replace />} />
                </Routes>
            </Suspense>
        </div>
    )
}

export default AppContent;
