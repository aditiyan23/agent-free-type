import "jquery/dist/jquery.min.js";
import "datatables.net-dt/js/dataTables.dataTables";
import "datatables.net-dt/css/jquery.dataTables.min.css";
import "datatables.net-buttons/js/dataTables.buttons.js";
import "datatables.net-buttons/js/buttons.colVis.js";
import "datatables.net-buttons/js/buttons.flash.js";
import "datatables.net-buttons/js/buttons.html5.js";
import "datatables.net-buttons/js/buttons.print.js";
import $ from "jquery";
import React from 'react';
import {Accordion, Button, Col, Dropdown, Form, Row} from "react-bootstrap";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faBriefcase, faEdit, faEye, faRefresh, faTrash} from "@fortawesome/free-solid-svg-icons";
require( 'jquery' );
require( 'jszip' );
require( 'pdfmake' );
require( 'datatables.net-dt' );
require( 'datatables.net-autofill-dt' );
require( 'datatables.net-buttons-dt' );
require( 'datatables.net-buttons/js/buttons.colVis.js' );
require( 'datatables.net-buttons/js/buttons.html5.js' );
require( 'datatables.net-buttons/js/buttons.print.js' );
require( 'datatables.net-keytable-dt' );
require( 'datatables.net-responsive-dt' );
require( 'datatables.net-rowgroup-dt' );
require( 'datatables.net-rowreorder-dt' );
require( 'datatables.net-searchbuilder-dt' );
require( 'datatables.net-searchpanes-dt' );
require( 'datatables.net-select-dt' );
require( 'datatables.net-staterestore-dt' );

const names = [
    {
        "id" : "1",
        "name" : "Service Fee",
        "desc" : "Description",
        "status" : 0
    },
    {
        "id" : "2",
        "name" : "Reissue Fee",
        "desc" : "Description",
        "status" : 0
    },
    {
        "id" : "3",
        "name" : "Cancellation Fee",
        "desc" : "Description",
        "status" : 3
    },
    {
        "id" : "4",
        "name" : "Refund Fee",
        "desc" : "Description",
        "status" : 3
    },
    {
        "id" : "5",
        "name" : "MDR Fee",
        "desc" : "Description",
        "status" : 1
    },
    {
        "id" : "6",
        "name" : "After Office Charge",
        "desc" : "Description",
        "status" : 3
    },
    {
        "id" : "7",
        "name" : "Late Payment",
        "desc" : "Description",
        "status" : 1
    },
    {
        "id" : "8",
        "name" : "Domestic Flight Service",
        "desc" : "Description",
        "status" : 1
    },
    {
        "id" : "9",
        "name" : "International Flight Service",
        "desc" : "Description",
        "status" : 1
    },
    {
        "id" : "10",
        "name" : "Domestic Hotel Service Fee",
        "desc" : "Description",
        "status" : 1
    },
    {
        "id" : "11",
        "name" : "Service Fee Other",
        "desc" : "Description",
        "status" : 1
    },
    {
        "id" : "12",
        "name" : "Reissue Fee Other",
        "desc" : "Description",
        "status" : 1
    },
    {
        "id" : "13",
        "name" : "Cancellation Fee Other",
        "desc" : "Description",
        "status" : 1
    },
    {
        "id" : "14",
        "name" : "Refund Fee Other",
        "desc" : "Description",
        "status" : 1
    },
    {
        "id" : "15",
        "name" : "MDR Fee Other",
        "desc" : "Description",
        "status" : 1
    },
    {
        "id" : "16",
        "name" : "After Office Charge Other",
        "desc" : "Description",
        "status" : 1
    },
    {
        "id" : "17",
        "name" : "Late Payment Other",
        "desc" : "Description",
        "status" : 1
    },
    {
        "id" : "18",
        "name" : "Domestic Flight Service Other",
        "desc" : "Description",
        "status" : 1
    },
    {
        "id" : "19",
        "name" : "International Flight Service Other",
        "desc" : "Description",
        "status" : 1
    },
    {
        "id" : "20",
        "name" : "Domestic Hotel Service Fee Other",
        "desc" : "Description",
        "status" : 1
    },
]


class Table extends React.Component {

    componentDidMount(){
        if (!$.fn.DataTable.isDataTable("#table")) {
            let table = $('#table').DataTable({
                language: {
                    lengthMenu: "Display -- records per page",
                    zeroRecords: "No matching Fee Type found",
                    infoEmpty: "No Fee Type found"
                },
                columnDefs: [
                {
                    orderable: false,
                    className: 'select-checkbox',
                    targets: 0
                },
                {
                    targets: 4,
                    render: function (data, type, row) {
                        if (data == 1) {
                            return 'Active';
                        } else if (data == 3) {
                            return 'Inactive';
                        } else if (data == 0) {
                            return 'Deleted';
                        }
                        // return original data if no matches
                        return data;
                    }
                },
                {  targets: 5,
                    render: function (data, type, row, meta) {
                        return '<input type="button" class="edit btn btn-primary bg-primary" id=n-"' + meta.row + '" value="Edit"/><input style="margin-left: 10px; margin-right: 10px" type="button" class="view btn btn-secondary bg-secondary" id=s-"' + meta.row + '" value="View"/><input type="button" class="delete btn btn-danger bg-danger" id=s-"' + meta.row + '" value="Delete"/>';
                    }

                },
                {
                    targets: 0,
                    render: function (data, type, row, meta) {
                        return type === "export" ? meta.row + 1 : data;
                    },
                }],
                select: {
                    style: 'os',
                    selector: 'td:first-child'
                },
                order: [[1, 'asc']],
                pagingType: "full_numbers",
                pageLength: 20,
                processing: true,
                dom: "Bfrtip",

                buttons: [
                    {
                        extend: "pageLength",
                        className: "btn btn-secondary bg-secondary",
                    },
                    // {
                    //     extend: 'collection',
                    //     text: 'Advance control',
                    //     className: "btn btn-secondary bg-secondary",
                    //     buttons: [
                    //         {
                    //             text: 'Toggle start date',
                    //             action: function ( e, dt, node, config ) {
                    //                 dt.column( -2 ).visible( ! dt.column( -2 ).visible() );
                    //             }
                    //         },
                    //         {
                    //             text: 'Toggle salary',
                    //             action: function ( e, dt, node, config ) {
                    //                 dt.column( -1 ).visible( ! dt.column( -1 ).visible() );
                    //             }
                    //         },
                    //     ]
                    // },
                    // {
                    //     extend: "copy",
                    //     className: "btn btn-secondary bg-secondary",
                    // },
                    {
                        extend: "csv",
                        text: "Download",
                        className: "btn btn-secondary bg-secondary",
                    },
                    {
                        extend: "print",
                        customize: function (win) {
                            $(win.document.body).css("font-size", "10pt");
                            $(win.document.body)
                                .find("table")
                                .addClass("compact")
                                .css("font-size", "inherit");
                        },
                        className: "btn btn-secondary bg-secondary",
                    }
                ],
                lengthMenu: [
                    [10, 20, 50, 100, -1],
                    [10, 20, 50, 100, "All"],
                ],
            });

            $.fn.dataTable.ext.search.push(
                function(settings, data) {
                    console.log(data);
                    // If checked and Position column is blank don't display the row
                    if (data[4] === 'Deleted') {
                        return false;
                    }

                    // Otherwise display the row
                    return true;
                }
            );
            table.draw();

            $.fn.dataTable.ext.search.pop();

            $('#table').on('click', '#select_all', function() {
                if ($('#select_all:checked').val() === 'on') {
                    table.rows().select();
                }
                else {
                    table.rows().deselect();
                }
            });

            $('#table tbody').on('click', '.edit', function () {
                var id = $(this).attr("id").match(/\d+/)[0];
                var data = $('#table').DataTable().row( id ).data();
                console.log('Edit', data);
            });

            $('#table tbody').on('click', '.view', function () {
                var id = $(this).attr("id").match(/\d+/)[0];
                var data = $('#table').DataTable().row( id ).data();
                console.log('View', data[1]);
            });

            $('#table').on('click', '.delete', function () {
                var table = $('#table').DataTable();
                table
                    .row($(this).parents('tr'))
                    .remove()
                    .draw();
            });


        }
    }

    showTable = () => {
        try {
            return names.map((item, index) => {
                return (
                    <tr key="{content}">
                        <td style={{textAlign: 'center'}}  key="{selection}"/>
                        <td style={{textAlign: 'center'}} key="{item.id}" className="text-xs font-weight-bold">{item.id}</td>
                        <td style={{textAlign: 'center'}}  key="{item.name}" className="text-xs font-weight-bold">{item.name}</td>
                        <td style={{textAlign: 'center'}}  key="{item.desc}" className="text-xs font-weight-bold">{item.desc}</td>
                        <td style={{textAlign: 'center'}}  key="{item.status}" className="text-xs font-weight-bold" >{item.status}</td>
                        <td style={{textAlign: 'center'}}  key="{action}" className="text-xs font-weight-bold">
                            <Button style={{width: '45px'}} data-toggle="tooltip" data-placement="top"
                                    title="Click to edit" href="#/master-data-management/fee-type/create">
                                <FontAwesomeIcon icon={faEdit} className="material-icons"/>
                            </Button>
                            <Button style={{width: '45px', marginLeft: '20px', marginRight: '20px'}}
                                    data-toggle="tooltip" data-placement="top" title="Click to view details">
                                <FontAwesomeIcon icon={faEye} className="material-icons"/>
                            </Button>
                            <Button style={{width: '45px'}} data-toggle="tooltip" data-placement="top"
                                    title="Click to delete">
                                <FontAwesomeIcon icon={faTrash} className="material-icons"/>
                            </Button>
                        </td>
                    </tr>
                );
            });
        } catch (e) {
            alert(e.message);
        }
    };

    render(){
        return(
            <div className="container-fluid py-4">
                <Accordion defaultActiveKey="0" flush>
                    <Accordion.Item eventKey="1">
                        <Accordion.Header>Advance Options</Accordion.Header>
                        <Accordion.Body>
                            <Row>
                                <Col>
                                    <Form>
                                        <Form.Label htmlFor="disabledSelect">Status</Form.Label>
                                        <Form.Select id="disabledSelect">
                                            <option>Active</option>
                                            <option>Inactive</option>
                                        </Form.Select>
                                    </Form>
                                </Col>
                                <Col/>
                                <Col/>
                                <Col/>
                                <Col/>
                                <Col/>
                                <Col style={{textAlign: 'right'}}>
                                    <FontAwesomeIcon style={{fontSize: "40px", color:"#333333"}} icon={faRefresh} className="material-icons"/>
                                </Col>
                            </Row>

                        </Accordion.Body>
                    </Accordion.Item>
                </Accordion>
                <br/>
                {/*<input type="checkbox" name="chk_box">Remove blank data</input>*/}
                <div className="table-responsive p-0 pb-2">
                    <table id="table" className="table align-items-center justify-content-center mb-0">
                        <thead>
                        <tr key="{header}">
                            <th key="{title1}" style={{textAlign: 'center'}}><input type="checkbox" id="select_all" name="select_invoice" /></th>
                            <th key="{title2}" style={{textAlign: 'center'}} className="text-uppercase text-secondary text-sm font-weight-bolder opacity-7 ps-2">Fee Type Code</th>
                            <th key="{title3}" style={{textAlign: 'center'}} className="text-uppercase text-secondary text-sm font-weight-bolder opacity-7 ps-2">Fee Type Name</th>
                            <th key="{title4}" style={{textAlign: 'center'}} className="text-uppercase text-secondary text-sm font-weight-bolder opacity-7 ps-2">Description</th>
                            <th key="{title5}" style={{textAlign: 'center'}} className="text-uppercase text-secondary text-sm font-weight-bolder opacity-7 ps-2">Status</th>
                            <th key="{title6}" style={{textAlign: 'center'}} className="text-uppercase text-secondary text-sm font-weight-bolder opacity-7 ps-2">Action</th>
                        </tr>
                        </thead>

                        <tbody>
                        {this.showTable()}
                        </tbody>
                    </table>
                </div>
            </div>
        )
    }
}

export default Table;