import "../assets/css/Helper.css";
import Tab from 'react-bootstrap/Tab';

import React from "react";
import { Formik } from "formik";
import * as Yup from "yup";
import {Card, Col, Nav, Row} from "react-bootstrap";

class Form extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            name: '',
            desc: '',
            text: '',
            language: '',
            checked: false
        };
        this.timer = null;
    }

    componentDidUpdate (prevProps, prevState) {
        if(prevState.text !== this.state.text) {
            this.handleCheck();
        }
    }

    onChange = (e, string) => {
        this.setState({
            text: e.target.value
        });
    };

    handleCheck = () => {
        // Clears running timer and starts a new one each time the user types
        clearTimeout(this.timer);
        this.timer = setTimeout(() => {
            this.toggleCheck();
            this.getLanguage(this.state.text)
        }, 1000);
    }

    toggleCheck = () => {
        this.setState( prevState => ({ checked: !prevState.checked }));
    }

    getLanguage(string) {
        // Simple POST request with a JSON body using fetch
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ q: string, api_key: "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx" })
        };
        fetch('https://libretranslate.de/detect', requestOptions)
            .then(response => response.json())
            .then(data => this.translate(data[0].language, string));
    }

    translate(language, string) {
        this.toZh(language, string);
        this.toId(language, string);
    }

    toZh(language, string) {
        // Simple POST request with a JSON body using fetch
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ q: string, source: language, target: "zh", api_key: "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx" })
        };
        fetch('https://libretranslate.de/translate', requestOptions)
            .then(response => response.json())
            .then(data => this.setState({ translatedTextZh: data.translatedText }));
    }

    toId(language, string) {
        // Simple POST request with a JSON body using fetch
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ q: string, source: language, target: "id", api_key: "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx" })
        };
        fetch('https://libretranslate.de/translate', requestOptions)
            .then(response => response.json())
            .then(data => this.setState({ translatedTextId: data.translatedText }));
    }

    render(){
        return <div className="app">
            <Formik
                initialValues={{ name: "", desc: "", code:"" }}
                onSubmit={async values => {
                    await new Promise(resolve => setTimeout(resolve, 500));
                    alert(JSON.stringify(values, null, 2));
                }}
                validationSchema={Yup.object().shape({
                    name: Yup.string()
                        .required("Required"),
                    desc: Yup.string()
                        .required("Required"),
                    code: Yup.string()
                })}
            >
                {props => {
                    const {
                        values,
                        touched,
                        errors,
                        dirty,
                        isSubmitting,
                        handleChange,
                        handleBlur,
                        handleSubmit,
                        handleReset
                    } = props;
                    return (
                        <div>
                            <form onSubmit={handleSubmit}>
                                <Row>
                                    <Col>
                                        <div>
                                            <label htmlFor="name" style={{ display: "block" }}>
                                                Fee Type Name
                                            </label>
                                            <input
                                                id="name"
                                                placeholder="Enter Fee Type Name"
                                                type="text"
                                                value={values.name}
                                                onChange={handleChange}
                                                onBlur={handleBlur}
                                                className={
                                                    errors.name && touched.name
                                                        ? "text-input error"
                                                        : "text-input"
                                                }
                                            />
                                            {errors.name && touched.name && (
                                                <div className="input-feedback">{errors.name}</div>
                                            )}
                                        </div>
                                        <br/>
                                        <div>
                                            <label htmlFor="desc" style={{ display: "block" }}>
                                                Description
                                            </label>
                                            <textarea
                                                rows="4" cols="50"
                                                id="desc"
                                                placeholder="Enter description"
                                                type="text"
                                                value={values.desc}
                                                onInput={event => this.onChange(event, values.desc)}
                                                onChange={handleChange}
                                                onBlur={handleBlur}
                                                className="form-control"

                                            />
                                            {errors.desc && touched.desc && (
                                                <div className="input-feedback">{errors.desc}</div>
                                            )}
                                        </div>
                                    </Col>
                                    <Col xs={1} md={1}/>
                                    <Col>
                                        <Card>
                                            <Card.Header>
                                                <div style={{padding: '10px'}}>
                                                    <span className="title-text">For Interface Purpose</span>
                                                    <label htmlFor="code" style={{ display: "block" }}>
                                                        Fee Type Code
                                                    </label>
                                                    <textarea
                                                        rows="4" cols="50"
                                                        id="desc"
                                                        placeholder="Enter Fee Type Code"
                                                        type="text"
                                                        value={values.code}
                                                        onChange={handleChange}
                                                        onBlur={handleBlur}
                                                        className="form-control"

                                                    />
                                                    {errors.code && touched.code && (
                                                        <div className="input-feedback">{errors.code}</div>
                                                    )}
                                                </div>
                                            </Card.Header>
                                        </Card>
                                    </Col>
                                </Row>

                                {/*<DisplayFormikState {...props} />*/}
                                <br/>
                                <span className="title-text">Translation</span>
                                <hr/>

                                <Tab.Container id="left-tabs-example" defaultActiveKey="ina">
                                    <Row>
                                        <Col sm={3}>
                                            <Nav variant="pills" className="flex-column">
                                                <Nav.Item>
                                                    <Nav.Link eventKey="ina" href="#">
                                                        Indonesian
                                                    </Nav.Link>
                                                </Nav.Item>
                                                <Nav.Item>
                                                    <Nav.Link eventKey="cn" href="#">
                                                        Chinese Simplified
                                                    </Nav.Link>
                                                </Nav.Item>
                                            </Nav>
                                        </Col>
                                        <Col sm={9}>
                                            <Tab.Content>
                                                <Tab.Pane eventKey="ina">
                                                    <div>
                                                        <label htmlFor="name" style={{ display: "block" }}>
                                                            Fee Type Name
                                                        </label>
                                                        <input
                                                            id="name"
                                                            placeholder="Enter Fee Type Name"
                                                            type="text"
                                                            value={props.values.name}
                                                            onChange={handleChange}
                                                            onBlur={handleBlur}
                                                            className={
                                                                errors.name && touched.name
                                                                    ? "text-input error"
                                                                    : "text-input"
                                                            }
                                                        />
                                                        {errors.name && touched.name && (
                                                            <div className="input-feedback">{errors.name}</div>
                                                        )}
                                                    </div>
                                                    <br/>
                                                    <div>
                                                        <label htmlFor="desc" style={{ display: "block" }}>
                                                            Description
                                                        </label>
                                                        <textarea
                                                            rows="4" cols="50"
                                                            id="desc"
                                                            placeholder="Enter description"
                                                            type="text"
                                                            value={this.state.translatedTextId}
                                                            onChange={handleChange}
                                                            onBlur={handleBlur}
                                                            className="form-control"

                                                        />
                                                        {errors.desc && touched.desc && (
                                                            <div className="input-feedback">{errors.desc}</div>
                                                        )}
                                                    </div>
                                                </Tab.Pane>
                                                <Tab.Pane eventKey="cn">
                                                    <div>
                                                        <label htmlFor="name" style={{ display: "block" }}>
                                                            Fee Type Name
                                                        </label>
                                                        <input
                                                            id="name"
                                                            placeholder="Enter Fee Type Name"
                                                            type="text"
                                                            value={values.name}
                                                            onChange={handleChange}
                                                            onBlur={handleBlur}
                                                            className={
                                                                errors.name && touched.name
                                                                    ? "text-input error"
                                                                    : "text-input"
                                                            }
                                                        />
                                                        {errors.name && touched.name && (
                                                            <div className="input-feedback">{errors.name}</div>
                                                        )}
                                                    </div>
                                                    <br/>
                                                    <div>
                                                        <label htmlFor="desc" style={{ display: "block" }}>
                                                            Description
                                                        </label>
                                                        <textarea
                                                            rows="4" cols="50"
                                                            id="desc"
                                                            placeholder="Enter description"
                                                            type="text"
                                                            value={this.state.translatedTextZh}
                                                            onChange={handleChange}
                                                            onBlur={handleBlur}
                                                            className="form-control"

                                                        />
                                                        {errors.desc && touched.desc && (
                                                            <div className="input-feedback">{errors.desc}</div>
                                                        )}
                                                    </div>
                                                </Tab.Pane>
                                            </Tab.Content>
                                        </Col>
                                    </Row>
                                </Tab.Container>

                                <button
                                    type="button"
                                    className="outline"
                                    onClick={handleReset}
                                    disabled={!dirty || isSubmitting}
                                >
                                    Reset
                                </button>
                                <button type="submit" disabled={isSubmitting}>
                                    Submit
                                </button>
                            </form>
                        </div>
                    );
                }}
            </Formik>
        </div>
    }
}

export default Form;
