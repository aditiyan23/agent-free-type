import React from 'react'
import AppSidebar from "../components/AppSidebar";
import AppHeader from "../components/AppHeader";
import AppContent from "../components/AppContent";
import AppBreadcrumb from "../components/AppBreadcrumb";
import AppFooter from "../components/AppFooter";

const DefaultLayout = () => {
  return (
      <div>
          <AppSidebar/>
          <div className="wrapper d-flex flex-column min-vh-100 bg-light">
              <AppHeader/>
              <div className="body flex-grow-1 main-content">
                  <AppBreadcrumb/>
                  <AppContent/>
                  <AppFooter/>
              </div>
          </div>
      </div>
  )
}

export default DefaultLayout
