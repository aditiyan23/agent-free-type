import React from 'react'
import Table from "../components/Table";
import {Link, useNavigate} from 'react-router-dom';
import {Button} from "react-bootstrap";

const FeeTypeList = () => {
    const navigate = useNavigate();

    const toComponentB=()=>{
        navigate('/master-data-management/fee-type/create',{state:{id:1,name:'sabaoon'}});
    }
    return (
        <div>
            <span className="title-text">Fee Type</span>
            <Button style={{float: 'right'}} variant="primary" onClick={()=>{toComponentB()}}>Create</Button>
            <Table/>
        </div>
    )
}

export default FeeTypeList