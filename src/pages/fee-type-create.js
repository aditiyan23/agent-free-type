import React from 'react'
import Form from "../components/Form";
import {Card} from "react-bootstrap";

const FeeTypeCreate = () => {
    return (
        <div>
            <span className="title-text">Create Fee Type</span>
            <Card style={{marginTop: "40px"}} body>
                <Form/>
            </Card>
        </div>
    )
}

export default FeeTypeCreate